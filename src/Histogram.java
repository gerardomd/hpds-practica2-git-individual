import java.util.HashMap;
import java.util.Map;

public class Histogram<T> {

    private final T [] data;
    Map<T,Integer> histogram;

    public Histogram(T[] data) {
        this.data = data;
        histogram = new HashMap<>();
        for (T value : data) {
            histogram.put(value, histogram.containsKey(value) ? histogram.get(value) + 1 : 1);
        }
    }

    public T[] getData() {
        return data;
    }

    public Map<T,Integer> getHistogram () {
        return histogram;
    }
    //Comentario para commit
}
